#include <stdio.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/* Good is "char array[][50]" or "char (*names)[50]"
 * but not "char **array" or "char *names[50]" */

void print_names1(char (*names)[50], int size)
{
	int i;

	for(i=0; i<size; i++)
		printf("%s\n", names[i]);
}

void print_names2(char names[][50], int size)
{
	int i;

	for(i=0; i<size; i++)
		printf("%s\n", names[i]);
}

void print_names3(char **names, int size)
{
	int i;

	for(i=0; i<size; i++)
		printf("%s\n", names[i]);
}

void print_names4(char *names[50], int size)
{
	int i;

	for(i=0; i<size; i++)
		printf("%s\n", names[i]);
}

int main() {
	char names[3][50] = {
		"Henry",
		"Molly",
		"Jack",
	};

	print_names1(names, ARRAY_SIZE(names));
	print_names2(names, ARRAY_SIZE(names));
	//print_names3(names, ARRAY_SIZE(names));
	//print_names4(names, ARRAY_SIZE(names));

	return 0;
}
