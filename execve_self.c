#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sched.h>

int main(int argc, char **argv, char **envp) {
	int pid, ret;
	char *arg[2];
	arg[0] = "native";
	arg[1] = NULL;

	if (argc > 1) {
		pid = fork();
		if (pid < 0) {
			perror("fork");
			return 1;
		} else if (pid == 0) {
			ret = unshare(CLONE_NEWNS);
			if (ret == -1) {
				perror("unshare");
				return 1;
			}
			ret = execve("/proc/self/exe", arg, NULL);
			if (ret == -1) {
				perror("execve");
				return 1;
			}
		} else {
			ret = wait(NULL);
			if (ret == -1) {
				perror("wait");
				return 1;
			}
			printf("Exited normally\n");
		}
	} else {
		printf("Executed\n");
	}
	return 0;
}
