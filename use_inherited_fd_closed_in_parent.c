#define _GNU_SOURCE
#include <stdio.h>
/* waitpid */
#include <sys/types.h>
#include <sys/wait.h>
/* open, fcntl */
#include <sys/stat.h>
#include <fcntl.h>
/* clone */
#include <sched.h>
/* sleep */
#include <unistd.h>

#define FILE_PATH "testfile"
#define TEST_STR "TEST_FROM_CLONE_FN"
#define CLONE_FLAGS CLONE_NEWUTS|CLONE_NEWPID|CLONE_NEWIPC|CLONE_NEWNET|CLONE_NEWNS|CLONE_NEWUSER|SIGCHLD

static int clone_fn(void *arg) {
	int ret;
	long fd = (long)arg;

	sleep(5);

	ret = write(fd, TEST_STR, sizeof(TEST_STR));
	if (ret == -1) {
		perror("write");
		return -1;
	}

	return 0;
}

int main() {
	long fd;
	int pid;
	char child_stack[4096 * 10];

	fd = open(FILE_PATH, O_WRONLY);
	if (fd == -1) {
		perror("open");
		return -1;
	}

	fcntl(fd, F_SETFD, FD_CLOEXEC);

	pid = clone(clone_fn, child_stack + sizeof(child_stack),
		CLONE_FLAGS, (void *) fd);
	if (pid < 0) {
		perror("clone");
		goto err;
	}

	close(fd);
	waitpid(pid, NULL, 0);
	return 0;
err:
	close(fd);
	return -1;
}
