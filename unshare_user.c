#define _GNU_SOURCE
#include <stdio.h>
#include <sched.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
	int ret;

	ret = unshare(CLONE_NEWPID|CLONE_NEWUSER);
	if (ret == -1) {
		perror("Failed to unshare CLONE_NEWPID|CLONE_NEWUSER");
		return 1;
	}
	ret = fork();
	if (ret == -1) {
		perror("Failed to fork");
		return 1;
	} else if (ret == 0) {
		ret = execv("dev_null", argv+1);
		if (ret == -1) {
			perror("Failed to execv");
			return 1;
		}
	} else {
		wait(&ret);
	}

	printf("%d %d, ret=%d\n", getpid(), getppid(), ret);
	return 0;
}
