CFLAGS += -Wall -Werror
all: dev_null unshare_user watchdog execve_self hidedir
dev_null: dev_null.c
unshare_user: unshare_user.c
watchdog: watchdog.c
execve_self: execve_self.c
hidedir: hidedir.c
use_inherited_fd_closed_in_parent: use_inherited_fd_closed_in_parent.c
pointer-to-array-syntax: pointer-to-array-syntax.c
