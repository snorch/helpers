#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#define TMP_MSG "Hello, World!"

int main()
{
	int ret;
	int first_fd_null, second_fd_null;
	char buf[100];

	first_fd_null = open("/dev/null", O_RDONLY);
	if (first_fd_null == -1) {
		perror("Failed to open /dev/null");
		return 1;
	}
	printf("Open /dev/null O_RDONLY fd = %d\n", first_fd_null);

	second_fd_null = open("/dev/null", O_RDWR);
	if (first_fd_null == -1) {
		perror("Failed to reopen /dev/null");
		return 1;
	}
	printf("Open second /dev/null O_RDWR fd = %d\n", second_fd_null);	

	ret = read(first_fd_null, buf, 10);
	if (ret == -1) {
		perror("Failed to read from /dev/null");
		return 1;
	} else if (ret != 0) {
		printf("Read something from /dev/null?\n");	
	}
	printf("Successfull read nothing from /dev/null\n");	

	ret = write(second_fd_null, TMP_MSG, strlen(TMP_MSG));
	if (ret == -1) {
		perror("Failed to write to /dev/null");
		return 1;
	}
	printf("Successfull write to /dev/null\n");	
	return 0;
}
