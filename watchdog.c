#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

int main()
{
	int ret, i, fd = open("/dev/watchdog", O_RDWR);
	if (fd == -1) {
		perror("open");
		return 1;
	}

	for (i = 0; i < 70; i+=10) {
		printf("%d\n", i);
		sleep(10);
	}

	ret = write(fd, "V", 1);
	if (ret == -1) {
		perror("write");
		return 1;
	}
	close(fd);
	return 0;
}
