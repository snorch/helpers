#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>

#define __NR_hidedir 511

long hidedir_syscall(const char *pathname) {
	return syscall(__NR_hidedir, pathname);
}

int main(int argc, char *argv[])
{
	long int ret;

	if (argc < 2) {
		printf("Please provide path\n");
	}

	ret = hidedir_syscall(argv[1]);
	if (ret == -1) {
		perror("Failed hidedir");
		return 1;
	}

	printf("Syscall returned %ld\n", ret);
	return 0;
}
